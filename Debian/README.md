# Installation d'un système Linux local pour les TPs

- Vérifiez que vous avez Virtual Box version 7.x
- Télécharger l'image iso : Debian netinstall version bookworm (12)

  https://www.debian.org/CD/netinst/

  pour amd64 (x86_64) : le fichier est : debian-12.2.0-amd64-netinst.iso

- Créez une nouvelle machine virtuelle, avec comme caractéristiques :

  * 2 CPUs
  * 4 Gio RAM (4096 Mio) voire 8 Gio (8192 Mio)
  * Sélectionner : "Skip unattended installation"
  * 20 Gio disque
  * Système UEFI
  * Dans stockage : associez le lecteur CD/DVD au fichier debian-12.2.0-amd64-netinst.iso
  * IMPORTANT (si l'installation de Debian se vautre) : 
    Dans la configuration de la machine virtuelle, Système | Accélération
    sélectionner "Héritage" (ou Legacy)

- Démarrez la VM, vous devriez voir un menu. Vous pouvez choisir
  "Graphical Install" (mode graphique) ou "Install" (mode texte)

- Langue français et clavier selon votre configuration
- Pour le compte utilisateur mettez votre prénom et un identifiant qui vous convient
- N'oubliez les mots de passe que vous indiquez pour root et cet utilisateur "normal"
- Utilisez tout le disque en mode assisté, une seule partition
- Sélectionnez à la fin "Environnement de bureau, Gnome, Serveur SSH"

## Finalisation de l'installation : pilotes Virtual Box

Dans le menu Virtual Box : Périphérique | Insérez l'image des additions invités

Dans Gnome : cliquez sur Activité, tapez Term, lancez un terminal (n'hésitez
pas à l'ajouter aux favoris du "Dock" pour un accès plus rapide).

_Note_ : si une commande échoue NE passez PAS à la suite !!!

~~~~Bash
$ whoami
votre_identifiant
$ su -
(donnez le mot de passe de root)
(ci-dessous mettez votre identifiant)
# usermod -a -G sudo votre_identifiant
# mount /media/cdrom
(warning sur read only, pas grave)
# apt -y install build-essential dkms
# bash /media/cdrom/VBoxLinuxAdditions.run
# reboot
~~~~

Au redémarrage vous devriez pouvoir activer le presse-papier partagé
(Périphérique | Presse-papier partagé | Bidirectionnel) et si vous
maximisez la fenêtre de Virtual Box, le bureau devrait occuper tout
l'écran.

Ouvrez un Terminal, testez que vous pouvez bien utiliser _sudo_ pour
exécuter une commande en tant que _root_ (administrateur) :

~~~~Bash
$ sudo whoami
(votre mot de passe utilisateur est demandé)
root
~~~~

## Examen des propriétés de nos VMs

Créez à nouveau une instance chacun.e en spécifiant votre
clef ssh publique, comme on l'a fait hier.

Toutes nos clefs SSH permettent de nous connecter à toutes
les instances !!! 

_Notes_ : les clefs publiques ssh permettant de se loguer
en root sont stockées dans `~/.ssh/authorized_keys`.
Vous pouvez empêcher les autres de se loguer en supprimant
toutes les lignes (sauf la vôtre !!!) dans ce fichier.




















