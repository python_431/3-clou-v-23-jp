# Déployer Wordpress avec Tofu sur une instance

Créer un plan de déploiement Tofu qui installe
Wordpress sur une instance.

Rappel : dans TP_wp il y a tout ce qui faut pour la
partie installation et configuration de Wordpress.

À la fin on doit pouvoir faire :

~~~~Bash
$ tofu init
$ tofu apply
... attendre un peu
~~~~

Et le Blog Wordpress est accessible au bout de qq minutes 
sur l'url http://ip_de_votre_srv

*Corrigé fourni dans ce répertoire : pensez à adapter les url des commande git clone ... et la variable confdir*

## Et sur deux instances ?

On peut envisager d'utiliser deux instances :

- Une pour le font Web (nginx, php, worpdress)
- L'autre pour la base de données MariaDB

Il faut bien penser à :

- Mettre l'IP _privée_ de la seconde instance dans la configuration `wp-config.php` de la première... Peut-on le faire dès le cloud-init ?
- Faire en sorte que le serveur mariadb soit contactable par le réseau (par défaut ce n'est pas le cas)
- Ne risque-t-on pas d'avoir mariadb accessible du monde entier, sachant qu'il est configuré avec des mots de passes faibles, ça pourrait être un problème !!!

À vous : créez un plan de déployement qui contient deux
instance serveur et qui installe uniquement la partie
Web sur la première (nginx, php, wordpress) et la partie
base de donnée sur la seconde.

Commencez par extraire chaque partie concerné du script initial pour
créer deux script cloud-init séparés.

Changement à prévoir dans le script qui install et
configure mariadb :

- Modifier le fichier de configuration `/etc/mysql/my.cnf` 
pour remplacer la ligne `# port 3306` par `port 3306`
(on décommente la ligne)
- Modifier la création de l'utilisateur wordpress_user :

~~~~SQL
GRANT ALL PRIVILEGES ON wordpress_db.*\
  TO 'wordpress_user'@'%' IDENTIFIED BY 'passw0rd';
FLUSH PRIVILEGES;
~~~~

Il reste le problème de devoir changer à la main dans
`wp-config.php` : `define( 'DB_HOST', 'localhost' );`
en `define( 'DB_HOST', 'ip_privée_de_l'autre_instance' );`

