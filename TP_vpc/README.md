# VPC et sécurisation dans le Cloud IaaS

Voici l'architecture recommandé pour un déploiement
d'application dans un cloud IaaS :

![](cloudvpc.drawio.png)

Vous pouvez travailler en équipe si vous le souhaitez.

# Première partie : montage du VPC avec la console Web

Avec la console Web montez et documentez une architecture similaire :

Un VPC, Deux "frontaux" Web avec apache2.

Mettez vos initiales en préfixe des ressources que vous créez pour
les retrouver plus facilement.

1. Création du VPC (le Private Network est créé dans la foulée)

On constate que la plage d'adresses IPv4 privées allouée est 
172.16.0.0/22

2. Création de deux instances (dans un premier temps on
leur donne une adresse IP publique)

(Si on ne peut pas passer par la console Web on peut cependant partir
du plan Tofu dans `TP_tf` pour créer deux instances)

Pensez bien à fournir un nom personnalisé avec votre nom d'équipe
en préfixe.

3. Associez vos deux instance au Virtual Network de votre VPC.

Avec le script "go" du répertoire Tf vérifiez que vous pouvez
vous connnecter en SSH à vos instances.

4. Ajoutez une gateway (à créer au préalable) à votre VPC et
activer le bastion associé à cette gateway. Notez l'adresse
IP publique de votre gateway.

Il faut rebooter les instances pour qu'elles aient une IP
dans le virtual network de notre VPC.

Tester le bastion :

~~~~Bash
$ ssh -J bastion@ip_publique_gateway:61000 root@nom_du_serveur
~~~~

par exemple chez moi : `ssh -J bastion@ip_pub_gateway:61000 root@jp-srv1` (ou 
jp-srv2)

À ce stade on peut supprimer les ip publiques des deux instances et vérifiez
que l'on peut toujour s'y connecter via le bastion.

Vérifiez que apache2 est bien installé sinon installez-le sur les deux
serveurs `apt -y install apache`.

Sur chacun des serveurs modifiez `/usr/share/apache2/default-site/index.html`
en ajoutant après "It works!" (dans la page Web) : `Ici nom_du_serveur`.

5. Notez les adresses IP privées de vos instance apache2, par exemple,
chez moi : 172.16.0.2 et 172.16.0.4.

Créez un load balancer et associez le à votre virtual network.
En passant par "créer et ajouter un front..." de type http, port
80 avec les deux IPs de nos serveurs comme "backend".

Examinez les propriété de votre load balancer et notez son ip
publique. Associez-le à votre private network.

6. Pour tester vous pouvez modifier la page HTML par défaut d'Apache2 
et vous connectez avec un navigateur à http://cette-ip/, a chaque
rechargement de la page le contenu sera fourni par l'un ou l'autre
serveur...

## Deuxième partie : construire la même architecture avec tofu

Commencez par supprimer toutes les ressources créées jusqu'ici.

Note : la conf qui marchait l'an dernier est dans `Tf_2022`

Point de départ un plan tofu/terraform qui monte un VPC avec
deux instances et un load balancer (répertoire `TP_vpc/Tf`) :

- `vpc.tf` : le VPC et le Virtual Network 
- `instances.tf` : les instances, sans ip publiques mais liée au vpc
- `lb.tf` : le load balancer

Comment utiliser ce plan ?

Commencer par copier dans un répertoire `Tf` tous les fichiers
de mon répertoire `TP_vpc/Tf` ; ensuite :

~~~~Bash
$ make init
Note: create a teamname.txt containing a nice name like 'farting-zuckerberg' (not that one!)
Type your team name (single word, e.g. funny-cat): NOM-DE-L'ÉQUIPE
$ make plan
$ make apply
$ ./go srv1
...srv1$ ip a
~~~~

Les cibles `make` définie dans `Makefile` permettent de gérer notre
déploiement, le script `go` simplifie la connexion à nos instances
via le bastion :

- make init : initialise tofu et le nom de l'équipe
- make plan : comme tofu plan
- make apply : comme tofu apply
- make destroy : comme tofu destroy
- make output : comme tofu output 

_Note_ : Les scripts cloud-init temporisent 60 secondes avant de
lancer les commandes apt : il y a un bug, semble-t-il, au niveau
réseau chez Scaleway : la connection avec internet n'est pas 
immédiatement possible (pb de gateway) ?

_Note 2_ : l'adresse IP fixe n'est pas attribuée au premier
démarrage, d'où la relance de l'interface réseau à la fin,
d'où le reboot à la fin (on devrait pouvoir faire mieux mais
la conf réseau des images scaleway est... bizarre)

Au bout de quelques minutes on peut tester que notre configuration
fonctionne : l'url `http://lb_ip/` (l'IP du Load Balancer peut
être obtenue avec `tofu output`) montre la page par défaut d'Apache
sur Debian.
