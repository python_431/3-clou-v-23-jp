#!/usr/bin/env bash

apt -y update && apt -y upgrade
apt -y install apache2

# HACK BIEN POURRI (à virer quand on aura trouvé comment
# fixer les IP privée dans le dhcp du VPC)
# en changeant de réseau (192.168.42.x au lieu de 10.42.42.x)
# ça marche sans ce hack...

#myip="$( scw-userdata myip)"
#
#cat > /etc/network/interfaces.d/99-hack<<EOF
#auto ens4
#iface ens4 inet static
#   address $myip
#   netmask 255.255.255.0 
#   metric 50
#EOF
#
#ifdown -a
#ifup -a

touch /root/cloud-init-ok
touch /tmp/cloud-init-ok

reboot
