# Client CLI (Command Line Interface) scw

Les opérateurs de Clous IaaS offrent tous une API
REST : à travers https, en étant authentifié et
en échangeant des flux au format JSON.

Divers outils simplifient l'utilisation de ces
API en ligne de commande.

- un outil en ligne de commande : `scw` (Scalewey)
 (`aws` chez Amazon, etc.)
- des outils de déploiement de plus haut niveau,
 le plus populaire est (était ?) Terraform.

On va installer et configurer pour utiliser nos
identifiants Scaleway le client `scw`.

1. Télécharger et installer le client `scw`
   (sur votre station de dev : GNU/Linux ou Windows)

Ref. https://www.scaleway.com/fr/cli/

~~~~Bash
$ curl  https://raw.githubusercontent.com/scaleway/scaleway-cli/master/scripts/get.sh | sudo sh
$ ls -l /usr/local/bin
scw
$ scw
~~~~

Documentation du CLI : https://www.scaleway.com/en/docs/compute/instances/api-cli/creating-managing-instances-with-cliv2/

Dans la console Web SCW :

Commencez par sélectioner le projet concerné : 2024-VER-B3.

À côté de votre icône de profil ouvrer le menu.

Cliquez sur "+ générez une clef API"

Laissez les paramètres par défaut et validez.

Cliquez sur "Ajoutez des clés API à votre environnement"

Copiez la ligne "scw init ..." complète avec le bouton "Copy"

Dans un terminal collez cette ligne, supprimez la partie
"-p newprofile" ce qui donne : 

~~~~Bash
$ scw init  \
  access-key=...votre clef...\
  secret-key=...votre secret...\
  organization-id=01548252-c96f-4826-a6cb-d7d596f52e93\
  project-id=a76a55c8-4679-4050-9dc5-5f1e22ac0fdd

Do you want to send usage statistics and diagnostics? (Y/n): n
Do you want to install autocomplete? (Y/n): y
What type of shell are you using (default: bash): 
We found an SSH key in ~/.ssh/id_rsa.pub. Do you want to add it to your Scaleway project? (Y/n):n
(parce que notre clef publique est déjà importée dans le projet)
$ scw instance server list
ID                                    NAME
75618ed7-cce1-49b2-8d7d-265c04b4aaf9  scw-gracious-cannonInesYTT
a00b7f87-617d-4c9a-9fbc-0b1e2d79e982  scw-vigilant-poincare
cbc37c0c-6ba5-46ce-88fe-9d25c48afbde  scw-ben-williamson
2b3e20b2-167e-4eb3-9cef-e3a556e22565  scw-Wer
~~~~

Pour info la configuration est stockée dans `~/.config/scw/config.yaml`

Trouvez dans la documentation comment créer une instance avec les
même caractéristiques que celles que nous avons déjà créées avec
la console Web en une seule ligne de commande et testez là.

cf. https://www.scaleway.com/en/docs/compute/instances/api-cli/creating-managing-instances-with-cliv2/

ou (plus précis) : https://github.com/scaleway/scaleway-cli/blob/master/docs/commands/instance.md#create-an-instance-image

## Example d'usage de scw CLI :

~~~~Bash
$ scw instance server create name=cli-votre_nom type=PRO2-XXS zone=fr-par-1 image=debian_bookworm
$ scw instance server list
-> notez bien l'ID de votre serveur
$ scw instance server get id_de_l'instance 
-> repérez l'IP publique affichée
$ ssh root@cette_ip
root@nom_de_l'instance...$ exit
$ scw instance server stop id_de_l'instance
... attendre un peu
$ scw instance server delete id_de_l'instance
$ scw instance server list id_de_l'instance
... elle n'est plus là
~~~~

Ajout d'un script utile : `go`. Ce script se connecte à
une instance dont on passe l'ID en argument (ou liste toutes
les instances du projet) en ssh en se débarrassant du
problème d'insertion de clefs publiques d'hôte dans le
fichier `~/.ssh/known_hosts` :

~~~~Bash
$ ./go
Usage : ./go instance_id
List of current instances :
ID                                    NAME       TYPE
421aa05c-dea0-45bd-a05a-9c7987feec5b  cli-jp-wp  PRO2-XXS
...
$ ./go 421aa05c-dea0-45bd-a05a-9c7987feec5b
...
Last login: Thu Dec  7 12:45:19 2023 from 46.247.227.178
root@cli-jp-wp:~# 
root@cli-jp-wp:~# exit
$
~~~~

TP : Reprenez les examples et scripts de ce répertoire
dans un répertoire de votre dépôt GIT et sur cette
base vérifiez que le déploiement totalement automatique
de Wordpress à partir d'un seul script `./deploy_wp`
à exécuter :

~~~~Bash
$ ./deploy_wp
on attend (on peut aller sur l'instance avec ./go
et attendre que `/tmp/wpok` existe...
~~~~

L'url http://ip_de_l'instance/ doit montrer une page
d'accueil "Blog Hexagone" 
