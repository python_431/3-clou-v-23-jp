# Problème d'obtention des clefs API

Depuis ce matin on n'arrive plus à obtenir des clefs
API (la console web dit que notre `user_id` n'existe pas !!!)

SI VOUS ÊTES CONCERNÉ SEULEMENT !!!

On peut utiliser la clef API d'Olivier (pas une très bonne
pratique, mais on a tous les même droits) :

- Dans le chat Wimi Olivier à poster qq lignes
- Copiez ces lignes dans `.config/scw/config.yaml` :

~~~~Bash
$ mkdir -p ~/.config/scw
$ chmod go= ~/.config/scw
$ nano ~/.config/scw
 copiez le contenu et enregistrez, pour tester :
$ scw instance server list
  ... nos instances ...
~~~~
